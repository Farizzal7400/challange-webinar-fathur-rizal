const express = require("express");
const router = express();

//database
const { UserGame, UserGameBiodata } = require("../models");

router.use(express.Router());
router.use(express.json());


router.get("/dashboard", (req, res, next) => {
   // MENGIRIMKAN LAGI PAKETNYA KE DATABASE
   UserGame.findAll().then((users) => {
   // BEBAS MAU ISI LOGIKA APA AJA. APAPUN ITU TERSERAH
     console.log(users[0].dataValues)
     res.render(`dashboard`, {
        users
     })
   })
//    UserGame.hasOne(UserGameBiodata, { foreignKey: "user_id", as: "UserGameBiodata" ,})
//    // UserGame.hasMany(UserGameHistory, { foreignKey: "user_id", as: "UserGameHistory" ,})
//    UserGame.findAll({
//       include: [
//          {
//             model: UserGameBiodata,
//             attributes: ["domisili", "email"],
//             as: "UserGameBiodata",
//             require: true
//          },
//       ],
//       // where: {username: "fafa"}
//    }).then(v => {
//       // let data = {
//       //    fullname: v[0].dataValues.fullname,
//       //    domisili: v[0].dataValues.UserGameBiodata.domisili
//       // }
//       console.log(v)
//       // console.log(data)
//    });
});


router.post("/dashboard", (req, res, next) => {
   UserGame.create({
      username: req.body.username,
      password: req.body.password,
      fullname: req.body.fullname,
      age: req.body.age,
      isActive: req.body.isActive
   }).then(value =>{
      // console.log(value)
      UserGameBiodata.create({
         user_id: value.dataValues.id,
         domisili: req.body.domisili,
         email: req.body.email
      })
   })
 });

module.exports = router;
