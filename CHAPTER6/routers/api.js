const express = require("express");
let users = require("../database/user.json");
const app = express();

//database
const { UserGame } = require("../models");

app.use(express.Router());
app.use(express.json());

// Get /user
app.get("/v1/user", (req, res, next) => res.json(users));
app.get("/v1/user/:id", (req, res, next) => {
  const user = users.find((item) => item.id === +req.params.id);
  if (user) {
    res.status(200).json(user);
  } else {
    res.status(200).send("ID not found");
  }
});

// Post /user
app.post("/v1/user", (req, res, next) => {
  const { name, email, password } = req.body;
  const id = users[users.length - 1].id + 1;
  const user = {
    id,
    name,
    email,
    password,
  };

  users.push(user);
  res.status(201).json(user);
});

app.put("/v1/user/:id", (req, res, next) => {
  let user = users.find((item) => item.id === +req.params.id);
  const params = {
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  };
  user = { ...user, ...params };
  users = users.map((item) => (item.id === user.id ? user : item));
  res.status(200).json(user);
});

app.delete("/v1/user/:id", (req, res, next) => {
  users = users.filter((item) => item.id !== +req.params.id);
  res.status(200).json({
    message: `Users id of ${req.params.id} has been deleted!`,
  });
});

//1
app.post("/v1/login/example", (req, res, next) => {
  // MENERIMA PAKET DARI FRONTEND ATAU WEB ATAU FORM
  const params = {
    username: req.body.username,
    password: req.body.password,
    fullname: req.body.fullname,
    age: req.body.age,
    isActive: true,
  };

  // MENGIRIMKAN LAGI PAKETNYA KE DATABASE
  UserGame.create({
    username: params.username,
    password: params.password,
    fullname: params.fullname,
    age: params.age,
    isActive: params.isActive
  }).then((value) => {
    // BEBAS MAU ISI LOGIKA APA AJA. APAPUN ITU TERSERAH
    UserGameBiodata.create({
        user_id: value.dataValues.id,
        domisili: req.body.domisili,
        email: req.body.email
        })
    }).then((user) => {
        console.log(user) 
        res.redirect("/login") //diubah kalau mau langsung direct
    })
});


//2
app.post("/v1/login/getuser", (req, res, next) => {
  // MENERIMA PAKET DARI FRONTEND ATAU WEB ATAU FORM
  console.log(req)
  const params = {
    id: req.body.id,
  };

  // MENGIRIMKAN LAGI PAKETNYA KE DATABASE
  UserGame.findOne({
    where: { id: params.id }
  }).then((user) => {
    // BEBAS MAU ISI LOGIKA APA AJA. APAPUN ITU TERSERAH
    console.log(user)
    res.redirect("/home/dashboard")
  })
});


module.exports = app;
