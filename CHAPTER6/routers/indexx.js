const express = require("express");
const res = require("express/lib/response");
const { password } = require("pg/lib/defaults");
const router = express.Router();

//baruch6pt3
const { UserGame } = require("../models");

// const bodyParser = require('body-parser');
// router.use(bodyParser.urlencoded());


// app.use(express.static(path.join(__dirname,"public")));

//CHAPTER 3 - HOMEPAGE
router.get("/", (req, res) => {
    res.render(`homepage.ejs`)
})

//LOGIN
router.get("/login", (req,res) => {
    res.render("login.ejs")
})

router.post("/tlogin", (req, res) =>{
    res.redirect("/login")
})


//CHAPTER 3 - HOME MENU
router.get("/home", (req,res) =>{
    res.render (`homemenu.ejs`)
})

router.post("/thome", (req, res) =>{
    res.redirect ("/home")
})

//CHAPTER 4 - GAME
router.get("/game", (req, res) => {
    res.render(`game.ejs`)
})

router.post("/togame", (req, res) =>{
    res.redirect("/game")
})

router.post("/login", (req, res, next) => {
    //tambahan ke2
    //menerima paket dari frontend / web / form
    const params = {
        username: req.body.username,
        password: req.body.password,
        fullname: req.body.fullname,
        age: req.body.age,
        isActive: true,
    };
    //mengirimkan lagi paketnya ke database
    UserGame.create({
        username: params.username,
        password: params.password,
        fullname: params.fullname,
        age: params.age,
        isActive: params.isActive,
        // isActive: params.isActive,
    }).then((user) => {
        //bebas mau isi logika apa saja, apapun itu 
        console.log(user)
    })
})

router.post("/v1/login/getuser", (req, res, next) => {
    // MENERIMA PAKET DARI FRONTEND ATAU WEB ATAU FORM
    console.log(req)
    const params = {
      id: req.body.id,
    };
  
    // MENGIRIMKAN LAGI PAKETNYA KE DATABASE
    UserGame.findOne({
      where: { id: params.id }
    }).then((user) => {
      // BEBAS MAU ISI LOGIKA APA AJA. APAPUN ITU TERSERAH
      console.log(user)
    })
});

module.exports = router