'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'UserGameBiodata',
      'user_id',
      Sequelize.INTEGER
    )
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'UserGameBiodata',
      'user_id'
    );
  }
};
