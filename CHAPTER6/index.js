
// const { Router } = require("express");

const express = require("express");
// const res = require("express/lib/response");
const path = require("path");

const app = express();

const indexRouter = require("./routers/index");
const gamesRouter = require("./routers/games");
const loginRouter = require("./routers/login");
const apiRouter = require("./routers/api");

const dashboardRouter = require("./routers/dashboard");
const signinRouter = require("./routers/signin");

// const router = express.Router();

const port = 9090;

const logger = (req,res, next) => {
    console.log(`${req.method} ${req.url}`)
    next()
}

app.set("view engine" , "ejs");

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(logger);
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname,"public")));
app.use("/css", express.static(path.join(__dirname + '/node_modules/bootstrap/dist/css')));
app.use("/js", express.static(path.join(__dirname + '/node_modules/bootstrap/dist/js')));

app.use("/", indexRouter);
app.use("/games", gamesRouter);
app.use("/login", loginRouter);
app.use("/api", apiRouter);
//baru
app.use("/home", dashboardRouter);
app.use("/signin", signinRouter);

app.use(logger);

const userManagement = require("./controller/UserManagement");
// const router = require("./routers/indexx")

app.use(userManagement);

// app.use(router);



// app.get("/", (req,res) => {
//     let name = req.query.name || "N/A"
//     res.render(`homepage.ejs`, {
//         name
//     })
// }) 


// app.get("/", (req,res) => {
//     let name = req.query.name || "N/A"
//     console.log(req.query.modal)
//     const show_modal =req.query.modal === "open" ? true : false; //cast to boolean
//     res.render(`homepage.ejs`, {
//         name, show_modal
//     })
// }) 

// app.get("/loginpage", (req,res) =>{
//     let name = req.query.name || "N/A"
//     res.render(`login.ejs`, {
//         name
//     })
// })

// app.post("/tlogin", (req,res) =>{
//     let name = req.query.name || "N/A"
//     res.render("/loginpage", {
//         name
//     })
// })

// app.get("/home", (req,res) => {
//     let name = req.query.name || "N/A"
//     res.render(`homemenu.ejs`, {
//         name
//     })
// })

// app.get("/game", (req,res) => {
//     let name = req.query.name || "N/A"
//     res.render(`game.ejs`, {
//         name
//     })
// }) 


//INTERNAL SERVER ERROR HANDLER
app.use((err, req, res, next) => {
    console.log(err)
    res.status(500).json({
        status: "fail",
        errors: err.message
    })
})


app.listen(port, () => console.log(`server Running! in port ${port}`))