const express = require("express");
const router = express();

router.use(express.Router());
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.get("/", (req, res, next) =>{
  const name = req.query.name;
  res.render(`gamez`, { title: "Game Page" })
});

router.post("/games", (req, res, next) => {
  console.log(req.query);
  res.status(200);

  res.render(`gamez`, { title: "Games"});
});

module.exports = router;
