
const pilihanHumanDisplay = document.getElementById("outputSihuman");
const pilihanRobotDisplay = document.getElementById("outputSirobot");
const hasilDisplay = document.getElementById("hasil1");
const pilihan = document.getElementsByName("btn_pick_human");
const robotBatu = document.getElementById("com_batu");
const robotKertas = document.getElementById("com_kertas");
const robotGunting = document.getElementById("com_gunting");
const humanBatu = document.getElementById("player_batu");
const humanKertas = document.getElementById("player_kertas");
const humanGunting = document.getElementById("player_gunting");
const ulangi = document.getElementById("fresh");

let pilihanHuman;
let pilihanRobot;
let hasil;


pilihan.forEach((pilihan) =>
  pilihan.addEventListener("click", (e) => {
    pilihanHuman = e.target.id;
    pilihanHumanDisplay.innerHTML = pilihanHuman;
    RobotRandom();
    gethasil();
  })
);

function RobotRandom() {
  const randomNumber = Math.floor(Math.random() * 3) + 1;

  if (randomNumber === 1) {
    pilihanRobot = "Batu";
    robotBatu.style.backgroundColor = "#ee6161";
  }
  if (randomNumber === 2) {
    pilihanRobot = "Kertas";
    robotKertas.style.backgroundColor = "#ee6161";
  }
  if (randomNumber === 3) {
    pilihanRobot = "Gunting";
    robotGunting.style.backgroundColor = "#ee6161";
  }

}

function gethasil() {
  if (pilihanRobot === pilihanHuman) {
    hasil = 'DRAW';
  }
  if (pilihanRobot === 'Batu' && pilihanHuman === 'Gunting') {
    hasil = 'LOSE';
  }
  if (pilihanRobot === 'Kertas' && pilihanHuman === 'Batu') {
    hasil = 'LOSE';
  }
  if (pilihanRobot === 'Gunting' && pilihanHuman === 'Kertas') {
    hasil = 'LOSE';
  }
  if (pilihanRobot === 'Gunting' && pilihanHuman === 'Batu') {
    hasil = 'WIN';
  }
  if (pilihanRobot === 'Batu' && pilihanHuman === 'Kertas') {
    hasil = 'WIN';
  }
  if (pilihanRobot === 'Kertas' && pilihanHuman === 'Gunting') {
    hasil = 'WIN';
  }
  hasilDisplay.innerHTML = hasil;
}


ulangi.addEventListener("click", function () {
    refreshPage();
  });
  

function refreshPage(refresh) {
    refresh = window.location.href = "/game";
    return refresh;
  }
  
  
// Sorry mas oop masih ga bisa jalan di saya jadi stuck, 
// dan meskipun data ini masih ada kendala juga di result