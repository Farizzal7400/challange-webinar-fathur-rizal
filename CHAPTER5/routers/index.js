const express = require("express");
const res = require("express/lib/response");
const router = express.Router();

// const bodyParser = require('body-parser');
// router.use(bodyParser.urlencoded());


// app.use(express.static(path.join(__dirname,"public")));

//CHAPTER 3 - HOMEPAGE
router.get("/", (req, res) => {
    res.render(`homepage.ejs`)
})

//LOGIN
router.get("/login", (req,res) => {
    res.render("login.ejs")
})

router.post("/tlogin", (req, res) =>{
    res.redirect("/login")
})


//CHAPTER 3 - HOME MENU
router.get("/home", (req,res) =>{
    res.render (`homemenu.ejs`)
})

router.post("/thome", (req, res) =>{
    res.redirect ("/home")
})

//CHAPTER 4 - GAME
router.get("/game", (req, res) => {
    res.render(`game.ejs`)
})

router.post("/togame", (req, res) =>{
    res.redirect("/game")
})

module.exports = router