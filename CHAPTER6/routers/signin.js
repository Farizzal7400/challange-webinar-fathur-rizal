const express = require("express");
const router = express();

router.use(express.Router());
router.use(express.json());

router.get("/", (req, res,) =>
  res.render(`signin`, { title: "SignIn Page" })
);

router.post("/signin", (req, res,) => {
    console.log(req.query);
    res.status(200);
});

module.exports = router;
